module.exports = (sequelize, DataTypes) => {
  return sequelize.define("cakes", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: DataTypes.STRING,
    price: DataTypes.FLOAT,
  });
};
