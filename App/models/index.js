const sequelize = require("./db.js");
const Cake = sequelize.import("./cakes.js");
sequelize.sync().then(() => {
  console.log("Database and tables ok");
});

module.exports = {
  sequelize,
  Cake,
};
