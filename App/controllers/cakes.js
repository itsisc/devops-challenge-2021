const Cake = require("../models/index").Cake;

const getCakes = async (req, res) => {
  const cakes = await Cake.findAll();
  if (cakes.length > 0) {
    res.status(200).json({ message: "Cakes offer found", cakes: cakes });
  } else {
    res
      .status(404)
      .json({ message: "There are currently no cakes in our offer" });
  }
};

const createCake = async (req, res) => {
  const cake = new Cake(req.body);
  if (cake.name && cake.price) {
    await cake.save();
    res.status(201).send({
      message: "Cake added!",
      cake: cake,
    });
  } else {
    res.status(500).json({
      message: "Invalid cake payload",
    });
  }
};

module.exports = { getCakes, createCake };
