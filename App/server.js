const express = require("express");
const bodyParser = require("body-parser");
const model = require("./models/index");
const indexRouter = require("./routes/cakes");
const path = require("path");
const cors = require("cors");

const app = express();
const port = 5000;
app.use(bodyParser.json());
app.use("/api", indexRouter);
app.use(cors());

app.use(express.static(path.resolve("./client")));

model.sequelize.sync();
app.listen(port, () => {
  console.log("Server started on port " + port);
});
